No Solution...

 Iteration:           2
 
Corrected P&T:
RAINFALL FORECAST:
 Below_Normal:   37.00000    
 Near_Normal:   36.00000    
 Above_Normal:   27.00000    
 
TEMPERATURE FORECAST:
 Below_Normal:   31.00000    
 Near_Normal:   38.00000    
 Above_Normal:   31.00000    
 
UNION P&T:
 UnionP_BN:   32.50000    
 UnionP_NN:   32.50000    
 UnionP_AN:   35.00000    
 
 UnionT_BN:   30.50000    
 UnionT_NN:   26.00000    
 UnionT_AN:   43.50000    
