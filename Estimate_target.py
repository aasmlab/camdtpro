import numpy as np
import matplotlib.pyplot as plt
import math
import calendar
from scipy import stats
from scipy.stats import kurtosis, skew
from numpy.linalg import inv
from scipy.stats import gamma
import pandas as pd
from scipy.optimize import curve_fit

def Estimate_Monthly_target (WTD_df,n_rep):
    #===================================================
    m_doys_list = [1,32,60,91,121,152,182,213,244,274,305,335]  #starting date of each month for regular years
    m_doye_list = [31,59,90,120,151,181,212,243,273,304,334,365] #ending date of each month for regular years
    m_doys_list2 = [1,32,61,92,122,153,183,214,245,275,306,336]  #starting date of each month for leap years
    #m_doye_list2 = [31,60,91,121,152,182,213,244,274,305,335,366] #ending date of each month for leap years
    m_doye_list2 = [31,59,91,121,152,182,213,244,274,305,335,366] #ignore 29th of Feb in case of leap year
    numday_list = [31,28,31,30,31,30,31,31,30,31,30,31]
    #======================================================================`
    year_array = WTD_df.YEAR.unique()
    nyears = year_array.shape[0]
    # wdir = "C:\\Users\\Eunjin\\IRI\\PH_FAO\\WGEN_PH\\"  #===> Hard coded *remove later
    rain_mat = np.empty([nyears, 12])*np.nan  #30 yrs of observation * 12 months
    tmax_mat = np.empty([nyears, 12])*np.nan  #30 yrs of observation * 12 months
    tmin_mat = np.empty([nyears, 12])*np.nan  #30 yrs of observation * 12 months
    srad_mat = np.empty([nyears, 12])*np.nan  #30 yrs of observation * 12 months

    for i in range(0,12):  #12 months
        Rain_Month= np.empty((nyears,numday_list[i],))*np.NAN  #e.g., 30 years * 31 days for January
        Tmax_Month= np.empty((nyears,numday_list[i],))*np.NAN
        Tmin_Month= np.empty((nyears,numday_list[i],))*np.NAN
        SRad_Month= np.empty((nyears,numday_list[i],))*np.NAN
        
        for j in range(0,nyears): #loop for extracting a matrix [31 days * n years] for each month
            if calendar.isleap(year_array[j]):
                Rain_Month[j,:] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= m_doys_list2[i]) & (WTD_df["DOY"] <= m_doye_list2[i])].values
                Tmin_Month[j,:] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= m_doys_list2[i]) & (WTD_df["DOY"] <= m_doye_list2[i])].values
                Tmax_Month[j,:] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= m_doys_list2[i]) & (WTD_df["DOY"] <= m_doye_list2[i])].values
                SRad_Month[j,:] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= m_doys_list2[i]) & (WTD_df["DOY"] <= m_doye_list2[i])].values
            else: #normal year
                Rain_Month[j,:] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= m_doys_list[i]) & (WTD_df["DOY"] <= m_doye_list[i])].values
                Tmin_Month[j,:] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= m_doys_list[i]) & (WTD_df["DOY"] <= m_doye_list[i])].values
                Tmax_Month[j,:] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= m_doys_list[i]) & (WTD_df["DOY"] <= m_doye_list[i])].values
                SRad_Month[j,:] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= m_doys_list[i]) & (WTD_df["DOY"] <= m_doye_list[i])].values
        #================================================================
        # MAKE 1) Monthly total rainfall and 2) mothly average temperature & srad
        #1)MAKE monthly SUM
        rain_mat[:,i] = np.sum(Rain_Month, axis=1) #row-wise sum
        tmax_mat[:,i] = np.mean(Tmax_Month, axis = 1)
        tmin_mat[:,i] = np.mean(Tmin_Month, axis = 1)
        srad_mat[:,i] = np.mean(SRad_Month, axis = 1)

    #===1) for rainfall amount : Loop for estimating 10 representative values for each month
    n_rep = 10
    target_rain = np.empty((n_rep,12,))* np.nan #initialize
    for i in range(12): #12 months
        rain_temp = rain_mat[:,i]
        #sort monthly rain from smallest to largest
        sorted_rain = np.sort(rain_temp)
        #======compute cdf for climatology
        fx_clim2 = [1.0/nyears] * nyears #pdf
        Fx_clim2 = np.empty((nyears,1)) #initialize
        for ii in range(0,nyears):
            if ii == 0:
                Fx_clim2[ii]=fx_clim2[ii]
            else:
                Fx_clim2[ii] = Fx_clim2[ii-1] + fx_clim2[ii]
        #===check if if any two or three months have same rain amount
        u, indices = np.unique(sorted_rain, return_index=True)
        u, count = np.unique(sorted_rain, return_counts=True)
        if len(u) < len(sorted_rain):
            for ii in range(len(count)):
                if count[ii] > 1: #if there is duplicate numbers
                    sum = 0
                    for iii in range(count[ii]):
                        sum = sum + Fx_clim2[indices[ii]+iii]
                    Fx_clim2[indices[ii]:(indices[ii]+count[ii])]= sum/float(count[ii])             
            
        #=Compute tangent(alpha) where alpha is a slope
        slope = np.empty((nyears,1))* np.nan #initialize
        for ii in range(0,nyears-1):
            slope[ii+1]= (Fx_clim2[ii+1] - Fx_clim2[ii])/(sorted_rain[ii+1]-sorted_rain[ii])
            if sorted_rain[ii+1] == sorted_rain[ii]:
                slope[ii+1] = np.nan
        # Extend the slope of the second segmenet to the first segment since we don't know for [F[x]=0 
        slope[0] = slope[1]

        #loop for 10 representatives
        target = np.linspace(0.05, 0.95, num=n_rep)
        for j in range(0,len(target)):
            if target[j] < Fx_clim2[0]:
                target_rain[j][i] = 2.5 #5 #arbitrary minimum monthly rainfall
            elif target[j] == Fx_clim2[0]:
                target_rain[j][i] = sorted_rain[0]
            else:
                t_rain = np.nan
                if target[j] <= Fx_clim2[0]: #first segment
                    t_rain = sorted_rain[0] - (Fx_clim2[0] - target[j])/slope[0]
                elif target[j] > Fx_clim2[nyears-2]: #last segment
                    t_rain = sorted_rain[nyears-1] - (Fx_clim2[nyears-1] - target[j])/slope[nyears-1]
                else: 
                    for k in range(0,nyears-1): #to find a right interval for a target Tavg
                        if target[j] > Fx_clim2[k] and target[j] <= Fx_clim2[k+1]:
                            t_rain = (target[j]- Fx_clim2[k])/slope[k+1] + sorted_rain[k]
                            break
                target_rain[j][i] = t_rain[0]
    #set minimum monthly rain as 5mm (arbitrary)
    #target_rain[target_rain < 3] = 3
    target_rain[target_rain < 2.5] = 2.5
    # fname = wdir + 'target_monthly_rain.csv' #*remove later
    # np.savetxt(fname, target_rain, fmt='%5.0f',delimiter=',')   # use exponential notation

    #=====shuffle 10 representative target rainfall for each month
    arr = np.arange(10)
    target_rain_shuffl = np.empty((n_rep,12,))* np.nan #initialize
    for i in range(12): #12 months
        np.random.shuffle(arr)
    ##    indx = np.copy(arr)
    ##    for j in range(len(indx)):
    ##        target_rain_shuffl[j,i] = target_rain[indx[j],i]
        for j in range(len(arr)):
            target_rain_shuffl[j,i] = target_rain[arr[j],i]
    # fname = wdir + 'target_monthly_rain_shuffled.csv' #*remove later           
    # np.savetxt(fname, target_rain_shuffl, fmt='%5.0f',delimiter=',')   # use exponential notation


    #====2) for Tmax : Loop for estimating 10 representative values for each month
    target_tmax = np.empty((n_rep,12,))* np.nan #initialize
    for i in range(12): #12 months
        tmax_temp = tmax_mat[:,i]
        #sort monthly rain from smallest to largest
        sorted_tmax = np.sort(tmax_temp)
        #======compute cdf for climatology
        fx_clim2 = [1.0/nyears] * nyears #pdf
        Fx_clim2 = np.empty((nyears,1)) #initialize
        for ii in range(0,nyears):
            if ii == 0:
                Fx_clim2[ii]=fx_clim2[ii]
            else:
                Fx_clim2[ii] = Fx_clim2[ii-1] + fx_clim2[ii]
        #===check if if any two or three months have same rain amount
        u, indices = np.unique(sorted_tmax, return_index=True)
        u, count = np.unique(sorted_tmax, return_counts=True)
        if len(u) < len(sorted_tmax):
            for ii in range(len(count)):
                if count[ii] > 1: #if there is duplicate numbers
                    sum = 0
                    for iii in range(count[ii]):
                        sum = sum + Fx_clim2[indices[ii]+iii]
                    Fx_clim2[indices[ii]:(indices[ii]+count[ii])]= sum/float(count[ii])             
            
        #=Compute tangent(alpha) where alpha is a slope
        slope = np.empty((nyears,1))* np.nan #initialize
        for ii in range(0,nyears-1):
            slope[ii+1]= (Fx_clim2[ii+1] - Fx_clim2[ii])/(sorted_tmax[ii+1]-sorted_tmax[ii])
            if sorted_tmax[ii+1] == sorted_tmax[ii]:
                slope[ii+1] = np.nan
        # Extend the slope of the second segmenet to the first segment since we don't know for [F[x]=0 
        slope[0] = slope[1]

        #loop for 10 representatives
        target = np.linspace(0.05, 0.95, num=n_rep)
        for j in range(0,len(target)):
            if target[j] < Fx_clim2[0]:
                target_tmax[j][i] = 2.5 #5 #arbitrary minimum monthly rainfall
            elif target[j] == Fx_clim2[0]:
                target_tmax[j][i] = sorted_tmax[0]
            else:
                t_rain = np.nan
                if target[j] <= Fx_clim2[0]: #first segment
                    t_rain = sorted_tmax[0] - (Fx_clim2[0] - target[j])/slope[0]
                elif target[j] > Fx_clim2[nyears-2]: #last segment
                    t_rain = sorted_tmax[nyears-1] - (Fx_clim2[nyears-1] - target[j])/slope[nyears-1]
                else: 
                    for k in range(0,nyears-1): #to find a right interval for a target Tavg
                        if target[j] > Fx_clim2[k] and target[j] <= Fx_clim2[k+1]:
                            t_rain = (target[j]- Fx_clim2[k])/slope[k+1] + sorted_tmax[k]
                            break
                target_tmax[j][i] = t_rain[0]
    #=====shuffle 10 representative target rainfall for each month
    arr = np.arange(10)
    target_tmax_shuffl = np.empty((n_rep,12,))* np.nan #initialize
    for i in range(12): #12 months
        np.random.shuffle(arr)
        for j in range(len(arr)):
            target_tmax_shuffl[j,i] = target_tmax[arr[j],i]
    # fname = wdir + 'target_monthly_tmax_shuffled.csv' #*remove later             
    # np.savetxt(fname, target_tmax_shuffl, fmt='%5.1f',delimiter=',')   # use exponential notation


    #====3) for Tmin : Loop for estimating 10 representative values for each month
    target_tmin = np.empty((n_rep,12,))* np.nan #initialize
    for i in range(12): #12 months
        tmin_temp = tmin_mat[:,i]
        #sort monthly rain from smallest to largest
        sorted_tmin = np.sort(tmin_temp)
        #======compute cdf for climatology
        fx_clim2 = [1.0/nyears] * nyears #pdf
        Fx_clim2 = np.empty((nyears,1)) #initialize
        for ii in range(0,nyears):
            if ii == 0:
                Fx_clim2[ii]=fx_clim2[ii]
            else:
                Fx_clim2[ii] = Fx_clim2[ii-1] + fx_clim2[ii]
        #===check if if any two or three months have same rain amount
        u, indices = np.unique(sorted_tmin, return_index=True)
        u, count = np.unique(sorted_tmin, return_counts=True)
        if len(u) < len(sorted_tmin):
            for ii in range(len(count)):
                if count[ii] > 1: #if there is duplicate numbers
                    sum = 0
                    for iii in range(count[ii]):
                        sum = sum + Fx_clim2[indices[ii]+iii]
                    Fx_clim2[indices[ii]:(indices[ii]+count[ii])]= sum/float(count[ii])             
            
        #=Compute tangent(alpha) where alpha is a slope
        slope = np.empty((nyears,1))* np.nan #initialize
        for ii in range(0,nyears-1):
            slope[ii+1]= (Fx_clim2[ii+1] - Fx_clim2[ii])/(sorted_tmin[ii+1]-sorted_tmin[ii])
            if sorted_tmin[ii+1] == sorted_tmin[ii]:
                slope[ii+1] = np.nan
        # Extend the slope of the second segmenet to the first segment since we don't know for [F[x]=0 
        slope[0] = slope[1]

        #loop for 10 representatives
        target = np.linspace(0.05, 0.95, num=n_rep)
        for j in range(0,len(target)):
            if target[j] < Fx_clim2[0]:
                target_tmin[j][i] = 5 #arbitrary minimum monthly rainfall
            elif target[j] == Fx_clim2[0]:
                target_tmin[j][i] = sorted_tmin[0]
            else:
                t_rain = np.nan
                if target[j] <= Fx_clim2[0]: #first segment
                    t_rain = sorted_tmin[0] - (Fx_clim2[0] - target[j])/slope[0]
                elif target[j] > Fx_clim2[nyears-2]: #last segment
                    t_rain = sorted_tmin[nyears-1] - (Fx_clim2[nyears-1] - target[j])/slope[nyears-1]
                else: 
                    for k in range(0,nyears-1): #to find a right interval for a target Tavg
                        if target[j] > Fx_clim2[k] and target[j] <= Fx_clim2[k+1]:
                            t_rain = (target[j]- Fx_clim2[k])/slope[k+1] + sorted_tmin[k]
                            break
                target_tmin[j][i] = t_rain[0]
    #=====shuffle 10 representative target rainfall for each month
    arr = np.arange(10)
    target_tmin_shuffl = np.empty((n_rep,12,))* np.nan #initialize
    for i in range(12): #12 months
        np.random.shuffle(arr)
        for j in range(len(arr)):
            target_tmin_shuffl[j,i] = target_tmin[arr[j],i]
    # fname = wdir + 'target_monthly_tmin_shuffled.csv' #*remove later             
    # np.savetxt(fname, target_tmin_shuffl, fmt='%5.1f',delimiter=',')   # use exponential notation


    #====4) for SRad : Loop for estimating 10 representative values for each month
    target_srad = np.empty((n_rep,12,))* np.nan #initialize
    for i in range(12): #12 months
        srad_temp = srad_mat[:,i]
        #sort monthly rain from smallest to largest
        sorted_srad = np.sort(srad_temp)
        #======compute cdf for climatology
        fx_clim2 = [1.0/nyears] * nyears #pdf
        Fx_clim2 = np.empty((nyears,1)) #initialize
        for ii in range(0,nyears):
            if ii == 0:
                Fx_clim2[ii]=fx_clim2[ii]
            else:
                Fx_clim2[ii] = Fx_clim2[ii-1] + fx_clim2[ii]
        #===check if if any two or three months have same rain amount
        u, indices = np.unique(sorted_srad, return_index=True)
        u, count = np.unique(sorted_srad, return_counts=True)
        if len(u) < len(sorted_srad):
            for ii in range(len(count)):
                if count[ii] > 1: #if there is duplicate numbers
                    sum = 0
                    for iii in range(count[ii]):
                        sum = sum + Fx_clim2[indices[ii]+iii]
                    Fx_clim2[indices[ii]:(indices[ii]+count[ii])]= sum/float(count[ii])             
            
        #=Compute tangent(alpha) where alpha is a slope
        slope = np.empty((nyears,1))* np.nan #initialize
        for ii in range(0,nyears-1):
            slope[ii+1]= (Fx_clim2[ii+1] - Fx_clim2[ii])/(sorted_srad[ii+1]-sorted_srad[ii])
            if sorted_srad[ii+1] == sorted_srad[ii]:
                slope[ii+1] = np.nan
        # Extend the slope of the second segmenet to the first segment since we don't know for [F[x]=0 
        slope[0] = slope[1]

        #loop for 10 representatives
        target = np.linspace(0.05, 0.95, num=n_rep)
        for j in range(0,len(target)):
            if target[j] < Fx_clim2[0]:
                target_srad[j][i] = 5 #arbitrary minimum monthly rainfall
            elif target[j] == Fx_clim2[0]:
                target_srad[j][i] = sorted_srad[0]
            else:
                t_rain = np.nan
                if target[j] <= Fx_clim2[0]: #first segment
                    t_rain = sorted_srad[0] - (Fx_clim2[0] - target[j])/slope[0]
                elif target[j] > Fx_clim2[nyears-2]: #last segment
                    t_rain = sorted_srad[nyears-1] - (Fx_clim2[nyears-1] - target[j])/slope[nyears-1]
                else: 
                    for k in range(0,nyears-1): #to find a right interval for a target Tavg
                        if target[j] > Fx_clim2[k] and target[j] <= Fx_clim2[k+1]:
                            t_rain = (target[j]- Fx_clim2[k])/slope[k+1] + sorted_srad[k]
                            break
                target_srad[j][i] = t_rain[0]


    #=====shuffle 10 representative target rainfall for each month
    arr = np.arange(10)
    target_srad_shuffl = np.empty((n_rep,12,))* np.nan #initialize
    for i in range(12): #12 months
        np.random.shuffle(arr)
        for j in range(len(arr)):
            target_srad_shuffl[j,i] = target_srad[arr[j],i]
    # fname = wdir + 'target_monthly_srad_shuffled.csv' #*remove later
    # np.savetxt(fname, target_srad_shuffl, fmt='%5.1f',delimiter=',')   # use exponential notation

    return target_rain_shuffl, target_tmax_shuffl, target_tmin_shuffl, target_srad_shuffl
