# camdtpro

CAMDT: simulating crop growth for Corn

A detailed report of this project can be found in the **corn-report.docx**

# Installation guide

1. Download and install [Anaconda](https://www.anaconda.com/products/individual) 
2. Downlaod this project as a zip file
3. unzip the file and place it in your C:\ drive
4. Open Windows PowerShell and type:
    ```
    conda init powershell
    ```
5. Type the following to go to the downloaded directory
    ```
    cd c:\camdtpro-master
    ```
6. Now create a new Python environment in the _camdt-pro_ directory:
    ```
    conda create -p ./camdt python=3.7
    ```
7. Activate the environment:
    ```
    conda activate C:\camdtpro-master\camdt
    ```
    
7. Install the requirements
    ```
    pip install -r requirements.txt
    ```
8. Now, you can safely run the GUI by typing:
    ```
    python C:\camdtpro-master\index.py
    ```
    
# Running CAMDT

After installing (see above), open Windows Powershell:
 - Go to the the project directory (step 5)
 - Activate the env (step 7)
 - And execute the script (step8)